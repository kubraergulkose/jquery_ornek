$(document).ready(function () {
    /*Bir Element Nasıl Seçilir? */
   // $(".yazilar").append("Kubra bu yazi metnine ekleme yapti.")
    // append komutunu metin eklemek için kullanıyoruz.// $("button").css({backgroundColor: "green"});
   // $("li:even").css( { color: "green"});
    // $("li:odd").css( { color: "red"});
   // $("li:nth-child(3)").css ({color:"red"});
  //  $("button:first-child").css({backgroundColor: "green"});
    var yazilar = $(".yazilar");
    $("#gizle").click(function () {
        yazilar.hide(500);
    });
    $("#goster").click(function () {
        yazilar.show(500);
    });
    $("#goster-gizle").click(function () {
        yazilar.toggle(500);
    });
    $("#acil-kapan").click(function () {
        yazilar.slideUp().slideDown();
    });

    $("#eleman-ekle").click(function () {
        yazilar.append("Sonuna ekleme yap - append ile");
        yazilar.prepend("basina ekleme yaptik - prepend ile");
    });
    $("#eleman-sil").click(function () {
        $("li:first-child").remove();
    });

    //////////////////////////////////////////////////7//////////////////////////////

    var kutu1 = $(".kutu1");
    var kutu2 = $(".kutu2");
    var poz1 = kutu1.position();
    var poz2 = kutu2.position();
    var sol1 = poz1.left;
    $("#anim-basla").click(function () {
       kutu1.css({position: "relative"});
        while(sol1<=1100)
        {
            kutu1.animate({
                left: "600"
            });
            sol1+=50;
        }
    })


});